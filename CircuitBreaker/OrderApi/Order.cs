﻿using System;
namespace OrderApi
{
    public class Order
    {
        public int Id{
            get;
            private set;
        }

        public string Products{
            get;
            private set;
        }

        public Order(int id, string products)
        {
            Id = id;
            Products = products;
        }
    }

    
}
