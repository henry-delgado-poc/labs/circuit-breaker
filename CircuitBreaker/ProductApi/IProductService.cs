﻿using System.Collections.Generic;

namespace CircuitBreaker.ProductApi
{
    public interface IProductService
    {
        public List<Product> GetProducts();
    }
}
