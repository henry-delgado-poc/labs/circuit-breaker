﻿using System;
using System.Collections.Generic;
using CircuitBreaker.ProductApi;

namespace ProductApi
{
    public class ProductService: IProductService
    {
        
        public List<Product> GetProducts()
        {
            var random = new Random().Next(1, 25);
            if (random > 10)
            {
                throw new Exception("An error has occurred pulling product data");
            }
            var list = new List<Product>();
            for (int i = 1; i <= 10; i++)
            {
                list.Add(new Product(i, $"PRO{i}", $"Product {i}", 100 *1.5  + i));
            }
            return list;
        }
    }
}
