using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CircuitBreaker.ProductApi;
using Microsoft.AspNetCore.Mvc;

namespace ProductApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet("GetProducts")]
        public ActionResult<List<Product>> GetProducts()
        {
            try
            {
                return _productService.GetProducts();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}