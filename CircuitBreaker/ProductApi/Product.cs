﻿
namespace CircuitBreaker.ProductApi
{
    public class Product
    {
        public int Id
        {
            get;
            private set;
        }

        public string Code{
            get;
            private set;
        }

        public string Name{
            get;
            private set;
        }

        public double Price{
            get;
            private set;
        }

        public Product(int id, string code, string name, double price)
        {
            Id = id;
            Code = code;
            Name = name;
            Price = price;
        }
    }
}
